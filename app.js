const express      = require('express');
require('./middlewares/security/passport');
const morgan       = require('morgan');
const bodyParser   = require('body-parser');
const mongoose     = require('mongoose');
const cors         = require('cors');
const config       = require('./middlewares/config');
const validator    = require('express-validator');
const acl          = require('./middlewares/permission/acl/acl');
const jwt          = require('jsonwebtoken');
const app          = express();
const passport     = require('passport');
const passportLogin = passport.authenticate('jwt', { session: false });

// Koneksi MongooDb 
// Only include useMongoClient only if your mongoose version is < 5.0.0
mongoose.connect(config.database,  err => {
  if (err) {
    console.log(err);
  } else {
    console.log('Connected to the database');
  }
});

// Definisi Route
const accountsRoutes      	= require('./routes/accounts');
const resetPasswordRoutes 	= require('./routes/reset-password');
const profileRoutes       	= require('./routes/profile');
const loginRoutes         	= require('./routes/login');
const settingsRoutes      	= require('./routes/settings');
const apiItemRoutes       	= require('./routes/api-item');
const apiCustRoutes       	= require('./routes/api-customer');
const apiPriceRoutes        = require('./routes/api-price');
const apiPaymentRoutes      = require('./routes/api-payment');
const forgotPasswordRoutes  = require('./routes/forgot-password');
const apiSalesOrderRoutes   = require('./routes/api-sales-order');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan('dev'));
app.use(cors());
app.use(validator());

app.get('/',(req,res)=>{
    res.send({'version': '1.3.2'});
});


// Route Login tanpa cek Permisiion dan Jwt
app.use('/login', loginRoutes);
app.use('/forgot-password', forgotPasswordRoutes);

// Cek JWt
app.use(passportLogin,function(req, res, next){
  // res.setHeader('Access-Control-Allow-Origin', "*");
  res.setHeader('Access-Control-Allow-Origin', "http://localhost:4200");
  // res.setHeader('Access-Control-Allow-Origin', "https://sales.uapps.id");
  // res.setHeader('Access-Control-Allow-Origin', "https://172.18.29.110");
  // res.setHeader('Access-Control-Allow-Origin', "https://172.18.29.121");
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type','Authorization',' Accept');
  req.decoded = req.user;
  next();
});

// Route Cek Jwt
app.use('/profile', profileRoutes);
app.use('/reset-password', resetPasswordRoutes);
app.use('/api-item', apiItemRoutes);
app.use('/api-customer', apiCustRoutes);
app.use('/api-price', apiPriceRoutes);
app.use('/api-sales-order', apiSalesOrderRoutes);
app.use('/api-payment', apiPaymentRoutes);
// Route cek Permission & Hak akses
acl.config({
  baseUrl: '/',
  filename:'acl.json',
  path:'middlewares/permission'
});
app.use(acl.authorize);

app.use('/accounts', accountsRoutes);
app.use('/settings', settingsRoutes);

// // Error handling ACL
// app.use(function(request, response, next) {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// app.use(function(err, request, response, next) {
//   response.json({
//       success: false,
//       message: 'Error.. You dont have permission || Failed Token || Something is wrong !!'
//   });

// });


app.listen(config.port, err => {
  console.log('Magic happens on port awesome ' + config.port);
});
