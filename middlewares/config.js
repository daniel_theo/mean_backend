const moment 		= require('moment-timezone');
const dateIndonesia = moment.tz(Date.now(), "Asia/Bangkok");

module.exports = {
 database:
   // "mongodb://172.18.29.122:27017/UnicoRayaMEAN",
   "mongodb://172.18.29.111:27017/UnicoRayaMEAN",
  	// port: 8081,
  	port: 3030,
  	indonesiaDate: dateIndonesia,
 	jwtSecret: 'UnicoRayaSecret@13!@#'
};
