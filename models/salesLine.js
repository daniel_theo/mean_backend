const mongoose    = require('mongoose');
const Schema      = mongoose.Schema;

const SalesLineSchema = new Schema({
	UAppsNo : { type: String, required: true, maxlength: 10 },
	type : { type: String, required: true, maxlength: 1, default: 0 },		//0 Item 1DevelieryService 2Coupon
	itemNo: { type: String, maxlength: 10 },
	UOM: { type: String, maxlength: 10 },
	description: { type: String, maxlength: 30 },
	description2: { type: String, maxlength: 30 },
	price :  Number,
	qty: Number,
	variantCode: { type: String, maxlength: 10 },
	locationCode: { type: String, maxlength: 10 },
	salesHeaderId : { type:Schema.Types.ObjectId, ref:'SalesHeader'}
});

module.exports = mongoose.model('SalesLine', SalesLineSchema);

