const mongoose    	= require('mongoose');
const Schema      	= mongoose.Schema;
const {indonesiaDate}        = require('../middlewares/config');

const SalesHeaderSchema = new Schema({
	UAppsNo : { type: String,  unique: true,required: true, maxlength: 10 },
	Top : { type: String, required: true, maxength: 10 },
	// coupon : Number,
	// couponName : { type: String, maxlength:30 },
	custCity : { type: String, maxlength:30 },
	custName : { type: String, maxlength:50 },
	custName2 : { type: String, maxlength:50 },
	custName2 : { type: String, maxlength:50 },
	custEmail : { type: String, maxlength:80 },
	custPhoneNo : { type: String, maxlength:50 },
	custNo : { type: String, maxlength:10, required: true },
	deliveryAddress :{
		code : { type: String, maxlength: 10 },
		Name : { type: String, maxlength: 50 },
		Name2 : { type: String, maxlength: 50 },
		Address : { type: String, maxlength: 50 },
		Address2 : { type: String, maxlength: 50 },
		City : { type: String, maxlength: 30 },
		Contact : { type: String, maxlength: 50 },
		PhoneNo: { type: String, maxlength: 30 },
		county : { type: String, maxlength: 10 },
	},
	// deliveryService : Number,
	discGroup : { type: String, required: true, maxlength: 10 },
	paymentMethod : {
		code : { type: String, maxlength: 10 },
		description : { type: String, maxlength: 30 },
		// bankNo : { type: String, maxlength: 30 },
		// name : { type: String, maxlength: 30 },
		// name2 : { type: String, maxlength: 30 },
	},
	totalAmount : Number,
	sales : { type:Schema.Types.ObjectId, ref:'User'},
	releaser : { type:Schema.Types.ObjectId, ref:'User'},
	salesperson : { type: String, maxlength: 50},
	company: { type: String, maxlength: 50},
 	dept_code: { type: String, maxlength: 50},
	notes:{type: String, maxlength:100 },
	status : { type: Number, default: 0},	//0(Open -U) 1(Open (N)) 2(Canceled) 3(Rejected) 4(Pending)	5(Approved(1)) 6(Approved(2)) 7(Released)
    created: { type: Date, default: indonesiaDate },

});

module.exports = mongoose.model('SalesHeader', SalesHeaderSchema);
