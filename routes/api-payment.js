"use strict";

const router        = require('express').Router();
const config        = require('../middlewares/config-sql');
const passport      = require('passport');
const passportLogin = passport.authenticate('jwt', { session: false });
const getData       = require('../helpers/getData');

let Connection      = require('tedious').Connection;
let Request         = require('tedious').Request;
let ConnectionPool  = require('tedious-connection-pool');

let connection      = new Connection(config);
let poolConfig = {
    min: 2,
    max: 8,
    log: false
};
let pool = new ConnectionPool(poolConfig, config);

pool.on('error', function(err) {
    console.error(err);
});

//Get Customer Pagination
router.route('/paymentMethod')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
     let db              = req.user.company;
    let rows             = [];
    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(`SELECT      A.Code 'code', 
                                               A.Description 'description',
                                               B.[Bank Account No_] 'bankNo',                                               
                                               B.[Name] 'name',
                                                 B.[Name 2] 'name2'
                                        FROM [${db}].[dbo].[${db}$Payment Method] A
                                            LEFT JOIN [${db}].[dbo].[${db}$Bank Account] B
                                        --ON A.[Bal_ Account No_] = B.No_
                                        ON A.[uApps Bank Account] = B.No_
                                       `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                payment: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

module.exports = router;