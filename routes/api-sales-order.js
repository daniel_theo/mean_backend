"use strict";
const router        = require('express').Router();
const config        = require('../middlewares/config-sql');
const passport      = require('passport');
const passportLogin = passport.authenticate('jwt', { session: false });
const getData       = require('../helpers/getData');
const async         = require("async");
const nodemailer    = require("nodemailer");
const path          = require('path');
const ABSPATH       = path.dirname(process.mainModule['filename']);
const SalesHeader   = require('../models/salesHeader');
const SalesLine     = require('../models/salesLine');

let Connection      = require('tedious').Connection;
let Request         = require('tedious').Request;
let ConnectionPool  = require('tedious-connection-pool');
let connection      = new Connection(config);
let poolConfig = {
    min: 2,
    max: 8,
    log: false,

};
let pool = new ConnectionPool(poolConfig, config);
var TYPES = require('tedious').TYPES;

pool.on('error', function(err) {
    console.error(err);
});

//so scrolling
router.route('/order-no')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db               = req.user.company;
    let rows             = [];
    let paramDesc        = req.query[Object.keys(req.query)[2]];
    let paramFix         = '';
    let paramGetData     = '';
    let weekStart = req.query[Object.keys(req.query)[0]];
    let weekEnd = req.query[Object.keys(req.query)[1]];

    if(/^(?!.*SALES)/.test(req.user.role) === true) {
        if(paramDesc == null ){
            paramFix   ='' 
        }else{
            paramGetData = getData.getParam(paramDesc);
            paramFix   =` AND A.No_+A.[Sell-to Customer Name]+A.[Sell-to Customer Name 2] LIKE '${paramGetData.toUpperCase()}'`
        }
    }else{

        let salesPersonCode = req.user.salesPerson; 
        if(salesPersonCode === undefined){ 
            salesPersonCode = '';
        } 

        if(paramDesc == null ){
            paramFix=` AND A.[Salesperson Code]= '${salesPersonCode}'`
        }else{
            paramGetData = getData.getParam(paramDesc);
            paramFix   =` AND A.[Salesperson Code]= '${salesPersonCode}' AND A.No_+A.[Sell-to Customer Name]+A.[Sell-to Customer Name 2] LIKE '${paramGetData.toUpperCase()}'`
        }
    }

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(
                                    `WITH SO AS
                                    (    SELECT  
                                            [RowNumber] = ROW_NUMBER() OVER (ORDER BY A.No_ ASC )

                                        FROM [${db}].[dbo].[${db}$Sales Header]A
                                        WHERE  A.[Document Type]= '1'
                                                AND A.[Document Date] BETWEEN '${weekStart}' AND '${weekEnd}'
                                                AND A.[Shortcut Dimension 1 Code] ='${dept_code}'
                                                AND A.Status <> 5 
                                        ${paramFix} 
                                    )
                                    SELECT top 1(SELECT TOP 1 [RowNumber] FROM SO ORDER BY [RowNumber] DESC) 'RowNumber'
                                    FROM SO A
                                    GROUP BY A.[RowNumber]
                                    ORDER BY A.[RowNumber]DESC
                                    `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                so: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

router.route('/orderScrolling')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db               = req.user.company;
    let rows             = [];
    let paramDesc        = req.query[Object.keys(req.query)[3]];
    let paramFix         = '';
    let paramGetData     = '';
    let weekStart = req.query[Object.keys(req.query)[1]];
    let weekEnd = req.query[Object.keys(req.query)[2]];
    let page             = parseInt(req.query[Object.keys(req.query)[0]]);

    if(/^(?!.*SALES)/.test(req.user.role) === true) {
        if(paramDesc == null ){
            paramFix   ='' 
        }else{
            paramGetData = getData.getParam(paramDesc);
            paramFix   =` AND A.No_+A.[Sell-to Customer Name]+A.[Sell-to Customer Name 2] LIKE '${paramGetData.toUpperCase()}'`
        }
    }else{

        let salesPersonCode = req.user.salesPerson; 
        if(salesPersonCode === undefined){ 
            salesPersonCode = '';
        } 

        if(paramDesc == null ){
            paramFix=` AND A.[Salesperson Code]= '${salesPersonCode}'`
        }else{
            paramGetData = getData.getParam(paramDesc);
            paramFix   =` AND A.[Salesperson Code]= '${salesPersonCode}' AND A.No_+A.[Sell-to Customer Name]+A.[Sell-to Customer Name 2] LIKE '${paramGetData.toUpperCase()}'`
        }
    }


    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(
                                    `WITH SO AS
                                    (    SELECT  
                                            [RowNumber] = ROW_NUMBER() OVER (ORDER BY A.No_ DESC ),
                                            A.No_ as 'DocNo'                                           
                                        FROM [${db}].[dbo].[${db}$Sales Header]A
                                        WHERE  A.[Document Type]='1'
                                                AND A.[Document Date] BETWEEN '${weekStart}' AND '${weekEnd}'
                                                AND A.[Shortcut Dimension 1 Code] ='${dept_code}'
                                                AND A.Status <> 5 
                                            ${paramFix} 
                                    )
                                    SELECT  A.DocNo,
                                            A.RowNumber,
                                            CASE 
                                                WHEN C.Status = 4 THEN 'Closed'
                                                WHEN C.Status = 0 THEN 'Open'
                                                WHEN C.Status = 1  THEN 'Released'
                                                WHEN C.Status = 2 THEN 'Pend. Approval'
                                                WHEN C.Status = 5 THEN 'Open Apps'
                                                ELSE 'Not Defined' END AS 'Status',
                                            C.[Sell-to Customer Name] + ' '+ C.[Sell-to Customer Name 2] as 'CustName',
                                            C.[Document Date] as 'Date'
                                            ,SUM(B.[Line Amount]) as 'Amount'
                                    FROM [${db}].[dbo].[${db}$Sales Line] B
                                        LEFT JOIN SO A
                                        ON A.DocNo = B.[Document No_]
                                        LEFT JOIN [${db}].[dbo].[${db}$Sales Header]C
                                        ON  B.[Document No_] = C.No_
                                    WHERE
                                        A.[RowNumber] BETWEEN (${page}) AND(${page}+19)
                                    GROUP BY      A.DocNo,
                                                A.RowNumber,
                                                C.Status,
                                                C.[Sell-to Customer Name] + ' '+ C.[Sell-to Customer Name 2],
                                                C.[Document Date]
                                    ORDER BY A.[RowNumber]ASC
                                    `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                salesOrder: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});



//Get SalesOrder Pagination
router.route('/sales-voucher')
.post(passportLogin,(req, res) => {
    let dept_code          = req.user.dept_code;
    let db                  = req.user.company;
    let rows                = [];
    let row                 = {};

    let dataItem             =[];
    req.body.forEach((item,i)=>dataItem.push([item.itemNo, item.qty, item.price, item.coupon, dept_code]));

    var TYPES = require('tedious').TYPES;
    var table = {
        columns: [
            {name: 'itemNO', type: TYPES.VarChar,length: 50},
            {name: 'qty', type: TYPES.Numeric, precision: 18, scale: 0},
            {name: 'price', type: TYPES.Int},
            {name: 'voucherNo', type: TYPES.VarChar,length: 10},
            {name: 'deptCode', type: TYPES.VarChar,length: 10},
        ],
        rows: dataItem
    };
    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }   
        var request = new Request(`[${db}].[dbo].[UAppsVoucher]`,
            function(err, rowCount) {
                if (err) {
                    console.log(err);
                    return
                } else {
                res.json({
                    success: true,
                    amount: rows,
                    message : "Successful"
                });
            }
            connection.release();
        });
        request.addParameter('tvp', TYPES.TVP, table);
        request.addOutputParameter('number', TYPES.Int);

        request.on('returnValue', function(parameterName, value, metadata) {
        row[parameterName] = value;
        rows.push(row);
    });
    connection.callProcedure(request);
    });
});

router.route('/sales-order')
.post(passportLogin,(req, res) => {
    
    let dept_code        = req.user.dept_code;
    // let user_name        = req.user.name.toUpperCase();
    let db               = req.user.company;
    let storeNo = "";
    let location = '';
    switch(req.user.company){
        case "SENTRALTUKANG":
            switch(dept_code){
                case "PDG" : 
                    storeNo = "S0001";
                    location = 'CB';
                break;
                case "PKU" :
                    storeNo = "PKU3";
                    location = 'PKU3';
                break;
            }
        break;
        case 'UNICODISTRIBUSI':
            switch(dept_code){
                case "PDG" : 
                    storeNo = "BP";
                    location = 'BP';
                break;
                case "PLM" :
                    storeNo = "PLM";
                    location = 'PLM';
                break;    
            }
        break;
        case 'KLASSEN':
            storeNo = 'S0005';
            location = 'JKT';
        break;
    }
    try{
        async.waterfall([
            function(done) {

                const invalidValues = [undefined, null, '', 0];
                let rows = [];
                let row = {};
                let custNo = req.body["custNo"];
                let top = req.body["Top"];
                let discGroup = req.body["discGroup"];
                let userName = req.user['name'].toUpperCase();
                let paymentMethod;
                let deliveryAddress = '';
                let salesPerson;
                let notes;
                let today = new Date();
                let currDate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

                let varCode = '';
                let SoNo = '';
                let output = '';
                (invalidValues.includes(req.body["paymentMethod"]))
                    ? paymentMethod = ''
                    : paymentMethod = req.body["paymentMethod"]["code"];

                (invalidValues.includes(req.body["deliveryAddress"]))
                    ? deliveryAddress = ''
                    : deliveryAddress = req.body["deliveryAddress"]["Code"];

                (invalidValues.includes(req.user["salesPerson"]))
                    ? salesPerson = ''
                    : salesPerson = req.user["salesPerson"];

                (invalidValues.includes(req.body["notes"]))
                    ? notes = ''
                    : notes = req.body["notes"];

                let dataItem = [];

                req.body["item"].forEach((item, i) => dataItem.push(
                    [0
                        , item.itemNo
                        , ''
                        , custNo
                        , top
                        , location
                        , dept_code
                        , SoNo
                        , item.qty
                        , item.price
                        , varCode
                        , deliveryAddress
                        , paymentMethod
                        , salesPerson
                        , userName
                        , notes
                        , discGroup
                        , storeNo
                        , 5
                        , ''
                        , currDate
                        , 0
                    ]
                ));

                //apakah ada delivery service atau tidak dan apabila delivery service = 0,ini tidak usah
                if (!invalidValues.includes(req.body["deliveryService"])) {
                    dataItem.push(
                        [1
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , 1
                            , req.body["deliveryService"]
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , 5
                            , ''
                            , currDate
                            , 0
                        ]
                    );
                };

                //apakah ada promo atau tidak dan apabila promo = 0,ini tidak usah

                if (!invalidValues.includes(req.body["coupon"] && req.body["couponName"])) {
                    dataItem.push(
                        [2
                            , ''
                            , req.body["couponName"]
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , 1
                            , req.body["coupon"]
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , 5
                            , ''
                            , currDate
                            , 0
                        ]
                    );
                };

                if (!invalidValues.includes(req.body["notes"])) {
                    dataItem.push(
                        [4
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , 0
                            , 0
                            , ''
                            , ''
                            , ''
                            , ''
                            , ''
                            , notes
                            , ''
                            , ''
                            , 0
                            , ''
                            , currDate
                            , 0
                        ]
                    );
                };

                let TYPES = require('tedious').TYPES;
                let table = {
                    columns: [
                        { name: 'type', type: TYPES.VarChar, length: 1 },
                        { name: 'itemNo', type: TYPES.VarChar, length: 10 },
                        { name: 'description', type: TYPES.VarChar, length: 30 },
                        { name: 'custNo', type: TYPES.VarChar, length: 20 },
                        { name: 'top', type: TYPES.VarChar, length: 10 },
                        { name: 'location', type: TYPES.VarChar, length: 10 },
                        { name: 'deptCode', type: TYPES.VarChar, length: 10 },
                        { name: 'soNo', type: TYPES.VarChar, length: 20 },
                        { name: 'qty', type: TYPES.Float },
                        { name: 'price', type: TYPES.Int },
                        { name: 'variantCode', type: TYPES.VarChar, length: 10 },
                        { name: 'deliveryAddress', type: TYPES.VarChar, length: 10 },
                        { name: 'paymentMethod', type: TYPES.VarChar, length: 10 },
                        { name: 'salesPerson', type: TYPES.VarChar, length: 10 },
                        { name: 'userName', type: TYPES.VarChar, length: 50 },
                        { name: 'notes', type: TYPES.VarChar, length: 80 },
                        { name: 'discGroup', type: TYPES.VarChar, length: 10 },
                        { name: 'storeNo', type: TYPES.VarChar, length: 10 },
                        { name: 'status', type: TYPES.Int },
                        { name: 'email', type: TYPES.VarChar, length: 50 },  
                        { name: 'releasedDate', type: TYPES.Date},                       
                        { name: 'discountLine', type: TYPES.Float }
                    ],
                    rows: dataItem
                };
                pool.acquire((err, connection) => {
                    if (err) {
                        console.error(err);
                        return;
                    }
                    let request = new Request(`[${db}].[dbo].[UAppsSaveSalesOrder]`,
                        (err, rowCount) => {
                            if (err) {
                                console.log(err);
                                return
                            } else {
                                
                                if(row["number"] === 0) {
                                    return res.status(400).send({
                                        success: false,
                                        message: 'Saving Failed'
                                    });	
                                    connection.release();
                                }else if(row["number"] === 1){
                                    done(err, row["soNo"]);
                                    connection.release();
                                }
                            }
                        });
                    request.addParameter('type', TYPES.Int, 0);
                    request.addParameter('tvp', TYPES.TVP, table);
                    request.addOutputParameter('number', TYPES.Int);
                    request.addOutputParameter('soNo', TYPES.VarChar);

                    request.on('returnValue', (parameterName, value, metadata) => {
                        row[parameterName] = value;
                        // rows.push(row);
                        // console.log(value);
                    });
                    connection.callProcedure(request);
                });

            },
            function(soValue, done) {
                // console.log(req.user, req.body);
                const months        = ["Januari", "Februari", "Maret","April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
                let nameUser        = req.user['name'];
                let namaCustomer    = req.body['custName'];
                let items           = req.body['item'];
                let dataItem        = "";
                let payment         = '';
                let delivery        = '';
                let voucherShow     = '';
                let headerDesain    = '';
                let footerDesain    = '';
                let deliveryService = 0;
                let voucher         = 0;
                let totalTransaction= 0;
                let dateCreate      = new Date();
                
                // Payment Description Condition
                (req.body.paymentMethod === undefined)
                    ?payment = '-'
                    :payment = req.body.paymentMethod['description'];

                // Detail Item Looping
                let totalPriceProduct   = items.reduce((acc, cur) => acc + (cur['qty'] * cur['price']), 0);
                for (let i = 0; i < items.length; i++) {
                    let no = i + 1;
                    dataItem += '<tr>'+
                                    '<td style="width:10px;">'+no+'</td>'+ 
                                    '<td colspan="2">'+items[i]['desc']+'</td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td></td>'+
                                    '<td>'+items[i]['qty']+' '+items[i]['UOM']+' &nbsp;&nbsp; <label style="color: rgba(0, 0, 0, 0.6);"> @'+items[i]['price'].toLocaleString(['id'])+'</label></td>'+
                                    '<td align="right">'+(items[i]['qty'] * items[i]['price']).toLocaleString(['id'])+'</td>'+
                                '</tr>';
                }

                // Header Design Condition
                if (req.user['company'] === 'UNICODISTRIBUSI') {
                    headerDesain =  '<div style="background: #c1c1c1;">'+
                                        '<table width="100%" height="39px;">'+
                                            '<tr>'+
                                                '<td valign="bottom" align="left">&nbsp;<img style="width: 80px; margin-top: 2px;" src="https://s3-ap-southeast-1.amazonaws.com/devpos.uapps.id/assets/logo/unico.png"></td>'+
                                                '<td valign="middle" align="right" width="130px;" style="font-size: 14px; line-height: auto; color: black; font-style: italic; font-weight: 500;">Order Submit &nbsp;</td>'+
                                            '</tr>'+
                                        '</table>'+
                                    '</div>'
                } else if (req.user['company'] === 'SENTRALTUKANG') {
                    headerDesain = '<div style="background: #ff4001;">'+
                                        '<table width="100%" height="39px;">'+
                                            '<tr>'+
                                                '<td valign="bottom" align="left">&nbsp;<img style="width: 115px; margin-top: 2px;" src="https://s3-ap-southeast-1.amazonaws.com/devpos.uapps.id/assets/logo/sentral-tukang.png"></td>'+
                                                '<td valign="middle" align="right" width="130px;" style="font-size: 14px; line-height: auto; color: #FFFFFF; font-style: italic;">Order Submit &nbsp;</td>'+
                                            '</tr>'+
                                        '</table>'+
                                    '</div>'
                } else {
                    headerDesain = '<div style="background: #443C3A;">'+
                                        '<table width="100%" height="39px;">'+
                                            '<tr>'+
                                                '<td valign="bottom" align="left">&nbsp;<img style="width: 115px; margin-top: 2px;" src="https://s3-ap-southeast-1.amazonaws.com/devpos.uapps.id/assets/logo/klassen.png"></td>'+
                                                '<td valign="middle" align="right" width="130px;" style="font-size: 14px; line-height: auto; color: #FFFFFF; font-style: italic;">Order Submit &nbsp;</td>'+
                                            '</tr>'+
                                        '</table>'+
                                    '</div>'
                }

                // Footer Design Condition
                if (req.user['company'] === 'UNICODISTRIBUSI') {
                    footerDesain = '<div style="font-size: 10px;text-align: center;color: rgba(0, 0, 0, 0.5);margin-top: 10px;">© 2019 PT Unico Sentral Distribusi. All Right Reserved</div>'
                } else if (req.user['company'] === 'SENTRALTUKANG') {
                    footerDesain = '<div style="font-size: 10px;text-align: center;color: rgba(0, 0, 0, 0.5);margin-top: 10px;">© 2019 PT Sentral Tukang Indonesia. All Right Reserved</div>'
                } else {
                    footerDesain = '<div style="font-size: 10px;text-align: center;color: rgba(0, 0, 0, 0.5);margin-top: 10px;">© 2019 PT Klassen Hardware Indonesia. All Right Reserved</div>'
                }

                // DeliveryService Condition
                if (req.body['deliveryService'] === undefined) {
                    deliveryService     = 0;
                    delivery            = '';
                } else {
                    deliveryService     = req.body['deliveryService'];
                    delivery            = '<tr><td align="right" style="color: rgba(0, 0, 0, 0.7);">Delivery</td><td align="right" style="width:100px;font-size: 12px;">'+deliveryService.toLocaleString(['id'])+'</td></tr>';
                }

                // Voucher Condition
                if (req.body['coupon'] === undefined) {
                    voucher             = 0
                    voucherShow         = '';
                } else {
                    voucher             = req.body['coupon']
                    voucherShow         = '<tr><td align="right" style="color: rgba(0, 0, 0, 0.7);">Voucher</td><td align="right" style="width:100px;font-size: 12px;">'+voucher.toLocaleString(['id'])+'</td></tr>';
                }
                totalTransaction        = totalPriceProduct + deliveryService - voucher;

                var smtpTransport = nodemailer.createTransport({
                    service: 'Gmail',
                    auth: {
                        user: 'notification.ur@gmail.com',
                        pass: 'notification@ur90'
                    }
                });
                var mailOptions = {
                    to: req.user.email,
                    from: 'notification.ur@gmail.com',
                    subject: 'Order Submit - ' +soValue,
                    html:    
                    // Design Email Notification
                    '<meta name="viewport" content="width=device-width, initial-scale=1.0">'+
                    '<div style="width: 330px; margin: 0 auto; border-radius: 0px 0px 5px 5px; background: rgba(196, 196, 196, 0.2); padding: 10px;">'+
                        headerDesain+
                        '<div style="background: #FFFFFF; padding: 10px;">'+
                            '<p style="color: #494949; font-size: 12px; margin-left: 3px;">Dear '+nameUser+',</p>'+
                            '<p style="color: #494949; font-size: 12px; margin-left: 3px;">This is to confirm that your order has been submitted through uApps Sales.</p>'+
                            '<table style="width: 100%; font-size: 12px">'+
                                '<tr>'+
                                    '<td style="font-size: 10px; color: rgba(0, 0, 0, 0.6);" align="left">Order No</td>'+
                                    '<td style="font-size: 10px; color: rgba(0, 0, 0, 0.6);" align="right">Order Date</td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td style="color: #494949;" align="left">'+soValue+'</td>'+
                                    '<td style="color: #494949;" align="right">'+dateCreate.getDate()+' '+months[dateCreate.getMonth()]+' '+dateCreate.getFullYear()+'</td>'+
                                '</tr>'+
                            '</table>'+
                            '<table style="width: 100%; font-size: 12px">'+
                                '<tr>'+
                                    '<td style="font-size: 10px; color: rgba(0, 0, 0, 0.6);" align="left">Customer Name</td>'+
                                    '<td style="font-size: 10px; color: rgba(0, 0, 0, 0.6);" align="right">Payment Method</td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td style="color: #494949;" align="left">'+namaCustomer+'</td>'+
                                    '<td style="color: #494949;" align="right">'+payment+'</td>'+
                                '</tr>'+
                            '</table>'+
                            '<p style="color: #000000; font-size: 14px; margin-left: 3px; font-weight: bold;">Order Detail</p>'+
                            '<table style="width:100%; font-size: 12px; color: #000000;">'+
                                dataItem+
                            '</table>'+
                            '<hr>'+
                            '<table style="width:100%;font-size: 10px;color: #000000;">'+
                            '<tr>'+
                                '<td align="right" style="color: color: #494949;">Subtotal</td>'+
                                '<td align="right" style="width:100px;font-size: 12px;">'+totalPriceProduct.toLocaleString(['id'])+'</td>'+
                            '</tr>'+
                            delivery+
                            voucherShow+
                            '<tr>'+
                                '<td align="right" style="color: #494949;">Total</td>'+
                                '<td align="right" style="width:100px; font-size:16px;">'+totalTransaction.toLocaleString(['id'])+'</td>'+
                            '</tr>'+
                            '</table>'+
                            '<br>'+
                            '<p style="font-size: 7px; text-align: center; color: #7A7A7A;">'+
                                'This is a computer-generated email. For further contact please refer to notification.ur@gmail.com'+
                            '</p>'+
                        '</div>'+
                        footerDesain+
                    '</div>',
                };
                smtpTransport.sendMail(mailOptions, function(err) {
                    res.json({
                        success: true,
                        soNumber: soValue,
                        message: 'Success order'
                    });
                    done(err, 'done');
                });
            }
        ], function(err) {
            if (err) return err;
        });
    }catch(err){
        let e = JSON.stringify(err, ["message", "arguments", "type", "name"]); 
        return {error: JSON.parse(e).message};
    } 
    
});

//so detail
router.route('/order-detail-header/:id')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db               = req.user.company;
    let rows             = [];

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(
                                    `
                                    SELECT      A.[Document Date]'Date',
                                                A.No_ 'DocNo',
                                                CASE 
                                                    WHEN A.Status = 4 THEN 'Closed'
                                                    WHEN A.Status = 0 THEN 'Open'
                                                    WHEN A.Status = 1  THEN 'Released'
                                                    WHEN A.Status = 2 THEN 'Pend. Approval'
                                                    WHEN A.Status = 5 THEN 'Open Apps'
                                                    ELSE 'Not Defined' END  'Status',
                                                B.[Name]  'custName',
                                                B.[Name 2]  'custName2',
                                                B.[City] 'custCity',
                                                B.[Phone No_]  'custPhoneNo',
                                                B.[E-Mail]'custEmail',
                                                C.[Name] 'shipName',
                                                C.[Name 2]  'shipName2',
                                                C.[Address]  'shipAddress',
                                                C.[Address 2] 'shipAddress 2',
                                                C.[City]'shipCity',
                                                C.[Phone No_] 'shipPhoneNo',
                                                
                                                D.[Description] 'paymentMethod'
                                        FROM [${db}].dbo.[${db}$Sales Header]A
                                        LEFT JOIN [${db}].dbo.[${db}$Customer] B
                                            ON A.[Sell-to Customer No_] = B.No_
                                        LEFT JOIN [${db}].dbo.[${db}$Ship-to Address]C
                                            ON A.[Ship-to Code] = C.Code
                                            AND A.[Sell-to Customer No_] = C.[Customer No_]
                                        LEFT JOIN [${db}].dbo.[${db}$Payment Method] D
                                            ON A.[Payment Method Code] = D.Code
                                        WHERE A.No_= '${req.params.id}'
                                    `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                header: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

router.route('/order-detail-comment/:id')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db               = req.user.company;
    let rows             = [];

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(
                                    `
                                    SELECT Comment
                                    FROM [${db}].dbo.[${db}$Sales Comment Line]A
                                    WHERE    A.[Document Type] =1
                                            AND No_ ='${req.params.id}'
                                    `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                comment: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

router.route('/order-detail-line/:id')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db               = req.user.company;
    // let rows             = [];

    async.series([
        (done)=> {
            let rows             = [];
            pool.acquire(function (err, connection) {
                if (err) {
                    console.error(err);
                    return;
                }
                var request = new Request(
                                            `
                                            SELECT    A.Description 'itemDesc'
                                                    , A.[Description 2] 'itemDesc2'
                                                    , A.Quantity 'itemQty'
                                                    , A.[Unit of Measure] 'itemUom'
                                                    , A.[Unit Price] 'itemPrice'
                                            FROM [${db}].dbo.[${db}$Sales Line]A

                                            WHERE    A.[Document Type]='1'
                                                    AND A.Type ='2'
                                                    AND A.[Document No_]='${req.params.id}'
                                            `
                    , function(err, rowCount) {
                    if (err) {
                        console.error(err);
                        return;
                    }
                    // res.json({
                    //     success: true,
                    //     item: rows,
                    //     message: "Successful"
                    // });
                    done(err, rows)
                    //release the connection back to the pool when finished
                    connection.release();
                });

                request.on('row', function(columns) {
                    var row = {};
                    columns.forEach(function(column) {
                        row[column.metadata.colName] = column.value;

                    });
                    rows.push(row);
                });
                connection.execSql(request);
            });
        }, (done)=> {
            let rows             = [];
            pool.acquire(function (err, connection) {
                if (err) {
                    console.error(err);
                    return;
                }
                var request = new Request(
                                            `
                                            SELECT    A.[Unit Price] 'deliveryPrice'
                                            FROM [${db}].dbo.[${db}$Sales Line]A

                                            WHERE    A.[Document Type]='1'
                                                    AND A.Type ='1'
                                                    AND A.[Document No_]='${req.params.id}'
                                                    AND A.No_='61013'

                                            `
                    , function(err, rowCount) {
                    if (err) {
                        console.error(err);
                        return;
                    }
                    // res.json({
                    //     success: true,
                    //     delivery: rows,
                    //     message: "Successful"
                    // });
                    done(err, rows)
                    //release the connection back to the pool when finished
                    connection.release();
                });

                request.on('row', function(columns) {
                    var row = {};
                    columns.forEach(function(column) {
                        row[column.metadata.colName] = column.value;

                    });
                    rows.push(row);
                });
                connection.execSql(request);
            });
        }, (done)=> {
            let rows             = [];

            pool.acquire(function (err, connection) {
                if (err) {
                    console.error(err);
                    return;
                }
                var request = new Request(
                                            `
                                            SELECT    A.Description 'itemDesc'
                                                    , A.[Unit Price] 'itemPrice'
                                            FROM [${db}].dbo.[${db}$Sales Line]A

                                            WHERE    A.[Document Type]='1'
                                                    AND A.Type ='1'
                                                    AND A.[Document No_]='${req.params.id}'
                                                    AND A.No_='44000'
                                            `
                    , function(err, rowCount) {
                    if (err) {
                        console.error(err);
                        return;
                    }
                    // res.json({
                    //     success: true,
                    //     item: item,
                    //     delivery: delivery,
                    //     coupon: rows,
                    //     message: "Successful"
                    // });
                    //release the connection back to the pool when finished

                    done(err,rows);
                    connection.release();
                });

                request.on('row', function(columns) {
                    var row = {};
                    columns.forEach(function(column) {
                        row[column.metadata.colName] = column.value;

                    });
                    rows.push(row);
                });
                connection.execSql(request);
            });
        }
    ],(err,results)=>{
        if(err)
            return res.json({
                success: false,
                message:err
        });

        res.json({
                success: true,
                item: results[0],
                delivery: results[1],
                coupon: results[2],
                message: "Successful"
        });
    });
});
// router.route('/order-detail-delivery-service/:id')
// .get(passportLogin,(req, res) => {
//     let dept_code        = req.user.dept_code;
//     let db               = req.user.company;
//     let rows             = [];

//     pool.acquire(function (err, connection) {
//         if (err) {
//             console.error(err);
//             return;
//         }
//         var request = new Request(
//                                     `
//                                     SELECT    A.[Unit Price] 'deliveryPrice'
//                                     FROM [${db}].dbo.[${db}$Sales Line]A

//                                     WHERE    A.[Document Type]='1'
//                                             AND A.Type ='1'
//                                             AND A.[Document No_]='${req.params.id}'
//                                             AND A.No_='61013'

//                                     `
//             , function(err, rowCount) {
//             if (err) {
//                 console.error(err);
//                 return;
//             }
//             res.json({
//                 success: true,
//                 delivery: rows,
//                 message: "Successful"
//             });
//             //release the connection back to the pool when finished
//             connection.release();
//         });

//         request.on('row', function(columns) {
//             var row = {};
//             columns.forEach(function(column) {
//                 row[column.metadata.colName] = column.value;

//             });
//             rows.push(row);
//         });
//         connection.execSql(request);
//     });
// });
// router.route('/order-detail-coupon/:id')
// .get(passportLogin,(req, res) => {
//     let dept_code        = req.user.dept_code;
//     let db               = req.user.company;
//     let rows             = [];

//     pool.acquire(function (err, connection) {
//         if (err) {
//             console.error(err);
//             return;
//         }
//         var request = new Request(
//                                     `
//                                    SELECT    A.Description 'itemDesc'
//                                             , A.[Unit Price] 'itemPrice'
//                                     FROM [${db}].dbo.[${db}$Sales Line]A

//                                     WHERE    A.[Document Type]='1'
//                                             AND A.Type ='1'
//                                             AND A.[Document No_]='${req.params.id}'
//                                             AND A.No_='44000'
//                                     `
//             , function(err, rowCount) {
//             if (err) {
//                 console.error(err);
//                 return;
//             }
//             res.json({
//                 success: true,
//                 coupon: rows,
//                 message: "Successful"
//             });
//             //release the connection back to the pool when finished
//             connection.release();
//         });

//         request.on('row', function(columns) {
//             var row = {};
//             columns.forEach(function(column) {
//                 row[column.metadata.colName] = column.value;

//             });
//             rows.push(row);
//         });
//         connection.execSql(request);
//     });
// });

module.exports = router;