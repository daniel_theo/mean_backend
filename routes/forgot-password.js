"use strict";

const router    = require('express').Router();
const User 		  = require('../models/user');
const config 	  = require('../middlewares/config');
const crypto    = require("crypto");
//Reset Password
router.route('/:token')
.get((req, res) => {
  User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, (err, user)=> {
      crypto.randomBytes(20, function(err, buf) {
        var token_password = buf.toString('hex');
      });
    user.old_password = user.password; 
  	user.password = this.token_password;
  	user.save();
    if (!user) {
      return res.json({
            success: false,
            message: [{msg:'Token is invalid or has expired'}]
      });
    }
    res.json({
              success: true,
              token: req.params.token,
              email: user.email
    });
  });
})
.post((req, res) => {
  req.checkBody('password','Password is Empty !!').notEmpty();
  req.checkBody('confirm','Passwords do not match.').equals(req.body.password);

  let errors = req.validationErrors();
    if (errors) {
      return res.json({
            success: false,
            message: errors
      });
  }

  User.findOne({ resetPasswordToken: req.params.token,  resetPasswordExpires: { $gt: Date.now() } }, (err, user) => {
      if (err) return   res.json({
            success: false,
            message: [{msg:'Failed '+ err}]
          });
      if (!user) {
        return res.json({
            success: false,
            message: [{msg:'Token is invalid or has expired'}]
          });
      } else if (user) {
        let validPassword = user.compareOldPassword(req.body.password);
        if (validPassword){
          return res.json({
              success: false,
              message: [{msg:'Password cannot same with last password'}]
          });
        }else{
          if(req.body.password !== req.body.confirm) {
            return res.json({
              success: false,
              message: [{msg:'Confirm password not Same'}]
            });
          }else{
            user.resetPasswordToken   = undefined;
            user.resetPasswordExpires = undefined;
            user.old_password         = undefined;
            user.password     	= req.body.password;
            user.save();
            res.json({
              success: true,
              message: 'Success Reset Password'
            });
          } 
        }  
      }
    });

});

module.exports = router;