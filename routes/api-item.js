"use strict";

const router        = require('express').Router();
const config        = require('../middlewares/config-sql');
const passport      = require('passport');
const passportLogin = passport.authenticate('jwt', { session: false });
const getData       = require('../helpers/getData');

let Connection      = require('tedious').Connection;
let Request         = require('tedious').Request;
let ConnectionPool  = require('tedious-connection-pool');

let connection      = new Connection(config);
let poolConfig = {
    min: 2,
    max: 8,
    log: false
};
let pool = new ConnectionPool(poolConfig, config);

pool.on('error', function(err) {
    console.error(err);
});

router.route('/list-all-item')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db              = req.user.company;
    let rows             = [];
    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(
                                     `WITH Item AS
                                    (
                                        SELECT  [RowNumber] = ROW_NUMBER() OVER (ORDER BY [Description] ASC ),
                                                [No_] as 'No',
                                                [Description] as 'Description',
                                                [Description 2] as 'Description2',
                                                [Base Unit of Measure] as 'UOM'
                                        FROM [${db}].[dbo].[${db}$Item]
                                     )
                                    SELECT  B.No,B.Description,B.Description2,B.RowNumber,B.UOM,
                                            (SELECT TOP 1 [RowNumber] FROM Item ORDER BY [RowNumber] DESC) 'COUNT',
                                            CONVERT(DECIMAL(10,2),ISNULL(SUM(A.Inventory),0))as Inv,
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('5',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0))) as 'InvToday',
                                            CONVERT(DECIMAL(10,2), 0) as 'InvTodaySODateNotToday',
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('10',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)))as 'ShipTodayToOutNotToday',
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('11',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)))as 'soQty',
                                            CONVERT(DECIMAL(10,2), 0) as So,
                                            CONVERT(DECIMAL(10,2), 0) as 'SoToday',
                                            CONVERT(DECIMAL(10,2),ISNULL(SUM(A.[Qty_ on Transfer Out]),0))as ToOut,
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('7',A.[Item No_],ISNULL(A.[Variant Code],'')  ,A.[Location Code],'',''),0)))as 'ToOutToday',
                                            CONVERT(DECIMAL(10,2),ISNULL(SUM(A.[Qty_ Sold not Posted]),0))as NotPosted,
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('8',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)))as 'NotPostedToday',
                                            AVG(ISNULL(C.[Price 1],0)) as 'Price1',
                                            AVG(ISNULL(C.[Price 2],0)) as 'Price2',
                                            AVG(ISNULL(C.[Price 3],0)) as 'Price3'
                                    FROM Item B
                                    INNER JOIN [${db}].[dbo].[UAppsActualStock]A
                                    ON B.No = A.[Item No_]
                                    LEFT JOIN [${db}].[dbo].[UAppsPrice]C
                                    ON A.[Item No_] = C.[Item No_] AND A.[Department Code] = C.[Dept Code]
                                    GROUP BY
                                        B.No, B.Description, B.Description2, B.[RowNumber], B.UOM
                                     ORDER BY
                                        B.[RowNumber]ASC
                                        `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                item: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

//Get Item Pagination
router.route('/list-item-scrolling-no')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db              = req.user.company;
    let rows             = [];
    let FetchRows        = 20;
    let paramDesc        = req.query[Object.keys(req.query)[0]];
    let paramGetData     = '';
    let paramFix         = '';

    if (paramDesc ==null){
        paramFix   =`WHERE [Blocked] = 0 AND [Description] <>''  AND [Description] <>'(NON AKTIF)' AND [Date Created] <> CONVERT(date, CAST(CONVERT(date, GETDATE()) AS datetime2)) `
    }else{
        paramGetData = getData.getParam(paramDesc);
        paramFix=`WHERE [Blocked] = 0 AND [Description] <>'' AND [Description] <>'(NON AKTIF)' AND [Description]+[Description 2]  LIKE '${paramGetData.toUpperCase()}' AND [Date Created] <> CONVERT(date, CAST(CONVERT(date, GETDATE()) AS datetime2)) `
     }

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(                         
                                     `WITH Item AS
                                    (
                                        SELECT  [RowNumber] = ROW_NUMBER() OVER (ORDER BY [No_] ASC )
                                        FROM [${db}].[dbo].[${db}$Item]
                                        ${paramFix} 
                                    )
                                     SELECT top 1(SELECT TOP 1 [RowNumber] FROM Item ORDER BY [RowNumber] DESC) 'RowNumber'
                                    FROM Item B
                                   
                                     GROUP BY
                                        B.[RowNumber]
                                   
                                     ORDER BY
                                        B.[RowNumber]DESC
                                        `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
             res.json({
                success: true,
                item: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

//Get Item Pagination
router.route('/list-item-scrolling')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db              = req.user.company;
    let rows             = [];
    let FetchRows        = 20;
    let page             = parseInt(req.query[Object.keys(req.query)[0]]);

    let paramDesc        = req.query[Object.keys(req.query)[1]];
    let paramGetData     = '';
    let paramFix         = '';

    if (paramDesc ==null){
        paramFix   =`WHERE [Blocked] = 0 AND [Description] <>''  AND [Description] <>'(NON AKTIF)' AND [Date Created] <> CONVERT(date, CAST(CONVERT(date, GETDATE()) AS datetime2)) `
    }else{
        paramGetData = getData.getParam(paramDesc);
        paramFix=`WHERE [Blocked] = 0 AND [Description] <>'' AND [Description] <>'(NON AKTIF)' AND [Description]+[Description 2]  LIKE '${paramGetData.toUpperCase()}' AND [Date Created] <> CONVERT(date, CAST(CONVERT(date, GETDATE()) AS datetime2)) `
     }

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(
                                 
                                     `WITH Item AS
                                    (
                                        SELECT  [RowNumber] = ROW_NUMBER() OVER (ORDER BY [Description] ASC ),
                                                [No_] as 'No',
                                                [Description] as 'Description',
                                                [Description 2] as 'Description2',
                                                [Base Unit of Measure] as 'UOM'
                                        FROM [${db}].[dbo].[${db}$Item]
                                        ${paramFix} 
                                    )
                                    SELECT  B.No,B.Description,B.Description2,B.RowNumber,B.UOM,
                                            (SELECT TOP 1 [RowNumber] FROM Item ORDER BY [RowNumber] DESC) 'COUNT',
                                            CONVERT(DECIMAL(10,2),ISNULL(SUM(A.Inventory),0))as Inv,
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('5',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)))as 'InvToday',
                                            CONVERT(DECIMAL(10,2), 0) as 'InvTodaySODateNotToday',
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('10',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)))as 'ShipTodayToOutNotToday',
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('11',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)))as 'soQty',
                                            CONVERT(DECIMAL(10,2), 0) as So,
                                            CONVERT(DECIMAL(10,2), 0) as 'SoToday',
                                            CONVERT(DECIMAL(10,2),ISNULL(SUM(A.[Qty_ on Transfer Out]),0))as ToOut,
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('7',A.[Item No_],ISNULL(A.[Variant Code],'')  ,A.[Location Code],'',''),0)))as 'ToOutToday',
                                            CONVERT(DECIMAL(10,2),ISNULL(SUM(A.[Qty_ Sold not Posted]),0))as NotPosted,
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('8',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)))as 'NotPostedToday',
                                            AVG(ISNULL(C.[Price 1],0)) as 'Price1',
                                            AVG(ISNULL(C.[Price 2],0)) as 'Price2',
                                            AVG(ISNULL(C.[Price 3],0)) as 'Price3'
                                    FROM Item B
                                    INNER JOIN [${db}].[dbo].[UAppsActualStock]A
                                    ON B.No = A.[Item No_]
                                    LEFT JOIN [${db}].[dbo].[UAppsPrice]C
                                    ON A.[Item No_] = C.[Item No_] AND A.[Department Code] = C.[Dept Code]

                                    WHERE

                                        [RowNumber] BETWEEN (${page}) AND(${page}+19)
                                        AND A.[Department Code]='${dept_code}'
                                    
                                    GROUP BY
                                        B.No, B.Description, B.Description2, B.[RowNumber], B.UOM
                                     ORDER BY
                                        B.[RowNumber]ASC
                                        `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
             res.json({
                success: true,
                item: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});


//Get Item Pagination
router.route('/list-item-pagination')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db              = req.user.company;
    let rows             = [];
    let FetchRows        = 20;
    let page             = parseInt(req.query[Object.keys(req.query)[0]]);
    let paramDesc        = req.query[Object.keys(req.query)[1]];
    let paramGetData     = '';
    let paramFix         = '';
    if (paramDesc ==null){
        paramFix   =`WHERE [Blocked] = 0 AND [Description] <>''  AND [Description] <>'(NON AKTIF)' AND [Date Created] <> CONVERT(date, CAST(CONVERT(date, GETDATE()) AS datetime2)) `
    }else{
        paramGetData = getData.getParam(paramDesc);
        paramFix=`WHERE [Blocked] = 0 AND [Description] <>'' AND [Description] <>'(NON AKTIF)' AND [Description]+[Description 2]  LIKE '${paramGetData.toUpperCase()}' AND [Date Created] <> CONVERT(date, CAST(CONVERT(date, GETDATE()) AS datetime2)) `
     }

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(                                 
                                     `WITH Item AS
                                    (
                                        SELECT  [RowNumber] = ROW_NUMBER() OVER (ORDER BY [Description] ASC ),
                                                [No_] as 'No',
                                                [Description] as 'Description',
                                                [Description 2] as 'Description2',
                                                [Base Unit of Measure] as 'UOM'
                                        FROM [${db}].[dbo].[${db}$Item]
                                        ${paramFix} 
                                    )
                                    SELECT  B.No,B.Description,B.Description2,B.RowNumber,B.UOM,
                                            (SELECT TOP 1 [RowNumber] FROM Item ORDER BY [RowNumber] DESC) 'COUNT',
                                            CONVERT(DECIMAL(10,2),ISNULL(SUM(A.Inventory),0))as Inv,
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('5',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)))as 'InvToday',
                                            CONVERT(DECIMAL(10,2), 0) as 'InvTodaySODateNotToday',
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('10',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)))as 'ShipTodayToOutNotToday',
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('11',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)))as 'soQty',
                                            CONVERT(DECIMAL(10,2), 0) as So,
                                            CONVERT(DECIMAL(10,2), 0) as 'SoToday',
                                            CONVERT(DECIMAL(10,2),ISNULL(SUM(A.[Qty_ on Transfer Out]),0))as ToOut,
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('7',A.[Item No_],ISNULL(A.[Variant Code],'')  ,A.[Location Code],'',''),0)))as 'ToOutToday',
                                            CONVERT(DECIMAL(10,2),ISNULL(SUM(A.[Qty_ Sold not Posted]),0))as NotPosted,
                                            CONVERT(DECIMAL(10,2),SUM(ISNULL([${db}].[dbo].UApps_Stock('8',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)))as 'NotPostedToday',
                                            AVG(ISNULL(C.[Price 1],0)) as 'Price1',
                                                                  AVG(ISNULL(C.[Price 2],0)) as 'Price2',
                                                                  AVG(ISNULL(C.[Price 3],0)) as 'Price3'
                                    FROM Item B
                                    INNER JOIN [${db}].[dbo].[UAppsActualStock]A
                                    ON B.No = A.[Item No_]
                                    LEFT JOIN [${db}].[dbo].[UAppsPrice]C
                                    ON A.[Item No_] = C.[Item No_] AND A.[Department Code] = C.[Dept Code]

                                    WHERE
                                        [RowNumber] BETWEEN ((${page} - 1) * ${FetchRows} + 1)
                                        AND (((${page} - 1) * ${FetchRows}) + ${FetchRows})
                                        AND A.[Department Code]='${dept_code}'
                                    
                                    GROUP BY
                                        B.No, B.Description, B.Description2, B.[RowNumber], B.UOM
                                     ORDER BY
                                        B.[RowNumber]ASC
                                        `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                item: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

//Get Item Detail
router.route('/list-item-detail/:_id')
.get(passportLogin,(req, res) => {
    let dept_code       = req.user.dept_code;
    let db              = req.user.company;
    let rows            = [];

    pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            //use the connection as normal
            var request = new Request(
                                   `
                                     WITH item as (
                                             SELECT 
                                                 A.[Item No_] as'No',
                                                 B.[Description] as 'Description',
                                                 B.[Description 2] as 'Description2',
                                              B.[Base Unit of Measure] as 'UOM',
                                                   A.[Department Code] as 'DeptCode',
                                                 CONVERT(DECIMAL(10,2),SUM(A.Inventory+
                                                  ISNULL([${db}].[dbo].UApps_Stock('5',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)-
                                                ISNULL([${db}].[dbo].UApps_Stock('10',A.[Item No_],ISNULL(A.[Variant Code],'')  ,A.[Location Code],'',''),0)-
                                                  ISNULL([${db}].[dbo].UApps_Stock('11',A.[Item No_],ISNULL(A.[Variant Code],'')  ,A.[Location Code],'',''),0)-
                                                  A.[Qty_ on Transfer Out]-
                                                  ISNULL([${db}].[dbo].UApps_Stock('7',A.[Item No_],ISNULL(A.[Variant Code],'')  ,A.[Location Code],'',''),0)-
                                                  A. [Qty_ Sold not Posted]-
                                                 ISNULL([${db}].[dbo].UApps_Stock('8',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)))'actualStock'
                                        FROM [${db}].[dbo].[UAppsActualStock]A
                                        LEFT JOIN [${db}].[dbo].[${db}$Item] B
                                          ON A.[Item No_] = B.[No_]


                                        WHERE [Item No_]= ${req.params._id} 
                                                AND [Department Code]= '${dept_code}'
                                        GROUP BY A.[Item No_], A.[Department Code], B.[Description], B.[Description 2],B.[Base Unit of Measure]
                                    )
                                    SELECT    A.No as'No',
                                            A.Description as 'Description',
                                            A.Description2 as 'Description2',
                                      A.UOM as'UOM',
                                                            A.actualStock AS 'actualStock',
                                                            ISNULL(B.[Price 1],0) as 'Price1',
                                                            ISNULL(B.[Price 2],0)  as 'Price2',
                                                            ISNULL(B.[Price 3],0)  as 'Price3'
                                    FROM item A
                                    LEFT JOIN [${db}].[dbo].[UAppsPrice] B
                                    ON A.No= B.[Item No_] AND
                                        A.[DeptCode] = B.[Dept Code]
                                    `



                                    , function(err, rowCount) {
                if (err) {
                    console.error(err);
                    return;
                }
                res.json({
                    success: true,
                    item: rows,
                    message: "Successful"
                });
                //release the connection back to the pool when finished
                connection.release();
            });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});


//Get Item Detail Variant
router.route('/list-item-detail-variant/:_id')
.get(passportLogin,(req, res) => {
    let dept_code       = req.user.dept_code;
    let db              = req.user.company;
    let rows            = [];

    pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            //use the connection as normal
            var request = new Request(                                          
                                     `SELECT  [Variant Code]  as'variantCode',
                                            [Item No_] as'No',
                                            --ISNULL([${db}].[dbo].[FN_AvgCost]('4','','','','',[Item No_],'${dept_code}'),0) as 'salesPrice',
                                            ISNULL([${db}].[dbo].[UApps_Price](0,'${dept_code}',[Item No_],'','',''),0)as 'salesPrice',
                                            CONVERT(DECIMAL(10,2), SUM(Inventory+
                                            ISNULL([${db}].[dbo].UApps_Stock('5',[Item No_],ISNULL([Variant Code],'') ,[Location Code],'',''),0)-
                                            [Qty_ on SO]-
                                            ISNULL([${db}].[dbo].UApps_Stock('6',[Item No_],ISNULL([Variant Code],'')  ,[Location Code],'',''),0)-
                                            [Qty_ on Transfer Out]-
                                            ISNULL([${db}].[dbo].UApps_Stock('7',[Item No_],ISNULL([Variant Code],'')  ,[Location Code],'',''),0)-
                                            [Qty_ Sold not Posted]-
                                            ISNULL([${db}].[dbo].UApps_Stock('8',[Item No_],ISNULL([Variant Code],'') ,[Location Code],'',''),0)))'actualStock'
                                    FROM [${db}].[dbo].[UAppsActualStock]

                                    WHERE [Item No_]=${req.params._id} 
                                            AND [Department Code]='${dept_code}'
                                    GROUP BY [Variant Code],[Item No_]`
                                    , function(err, rowCount) {
                if (err) {
                    console.error(err);
                    return;
                }
                res.json({
                    success: true,
                    item: rows,
                    message: "Successful"
                });
                //release the connection back to the pool when finished
                connection.release();
            });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

//Get Item Detail Gudang
router.route('/list-item-detail-gudang/:_id')
.get(passportLogin,(req, res) => {
    let dept_code       = req.user.dept_code;
    let db              = req.user.company;
    let rows            = [];

    pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            //use the connection as normal
            var request = new Request(                                
                                     `
                                     WITH itemLoc as (
                                             SELECT 
                                                [Item No_] as'No',
                                                [Department Code] as 'DeptCode',
                                                CONVERT(DECIMAL(10,2),SUM(ISNULL(Inventory, 0)+
                                                ISNULL([${db}].[dbo].UApps_Stock('5',[Item No_],ISNULL([Variant Code],'') ,[Location Code],'',''),0)-
                                                ISNULL([Qty_ on SO] ,0)-
                                                ISNULL([${db}].[dbo].UApps_Stock('6',[Item No_],ISNULL([Variant Code],'') ,[Location Code],'',''),0)-
                                                ISNULL([Qty_ on Transfer Out] ,0)-
                                                ISNULL([${db}].[dbo].UApps_Stock('7',[Item No_],ISNULL([Variant Code],'') ,[Location Code],'',''),0)-
                                                ISNULL([Qty_ Sold not Posted] ,0)-
                                                ISNULL([${db}].[dbo].UApps_Stock('8',[Item No_],ISNULL([Variant Code],'') ,[Location Code],'',''),0)))as 'actualStock'
                                        FROM [${db}].[dbo].[UAppsActualStock]


                                        WHERE [Item No_]= ${req.params._id} 
                                                AND [Department Code]= '${dept_code}'
                                        GROUP BY [Item No_],[Department Code]
                                    )
                                    SELECT    A.No as'No',
                                                        A.actualStock AS 'actualStock',
                                                        ISNULL(B.[Price 1],0) as 'Price1',
                                                        ISNULL(B.[Price 2],0) as 'Price2',
                                                        ISNULL(B.[Price 3],0) as 'Price3'
                                    FROM itemLoc A
                                    LEFT JOIN [${db}].[dbo].[UAppsPrice] B
                                    ON A.No= B.[Item No_] AND
                                        A.[DeptCode] = B.[Dept Code]
                                    `
                                    , function(err, rowCount) {
                if (err) {
                    console.error(err);
                    return;
                }
                res.json({
                    success: true,
                    item: rows,
                    message: "Successful"
                });
                //release the connection back to the pool when finished
                connection.release();
            });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

//Get Item Detail Gudang
router.route('/list-incoming-item/:_id')
.get(passportLogin,(req, res) => {
    let dept_code       = req.user.dept_code;
    let db              = req.user.company;
    let rows            = [];

    pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            //use the connection as normal
            var request = new Request(     `SELECT   0 'type',
                                                    CONVERT(VARCHAR(10),[Expected Receipt Date], 103)  as 'Tgl', 
                                                    CONVERT(DECIMAL(10,2),[Outstanding Quantity]) as 'OutsQty'
                                             FROM [${db}].[dbo].[${db}$Purchase Line]

                                        WHERE   [Document Type] ='1'
                                            AND [No_] = ${req.params._id}
                                            AND [Shortcut Dimension 1 Code] = '${dept_code}'
                                            AND [PO Status]<>'4' 

                                        UNION ALL

                                        SELECT      1 'type',
                                                    A.[Document No_],
                                                    CONVERT(DECIMAL(10,2),SUM(A.[Outstanding Quantity])) as 'OutsQty'
                                            FROM [${db}].[dbo].[${db}$Transfer Line] A
                                                LEFT JOIN [${db}].[dbo].[${db}$Transfer Header] B
                                            ON A.[Document No_] = B.No_
                                            WHERE   A.[Item No_] = ${req.params._id}
                                                    AND B.Status <>2
                                                    AND A.[Outstanding Quantity]<>0
                                                    --AND A.[Transfer-from Code]<>'INTRANSIT'
                                                    AND B.[Transfer-to Depart_ Code] ='${dept_code}'
                                            GROUP BY A.[Document No_]`
                                    , function(err, rowCount) {
                if (err) {
                    console.error(err);
                    return;
                }
                res.json({
                    success: true,
                    item: rows,
                    message: "Successful"
                });
                //release the connection back to the pool when finished
                connection.release();
            });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

//Cek Data Stock
router.route('/cek-stock-item')
.get(passportLogin,(req, res) => {
    let dept_code       = req.user.dept_code;
    let db              = req.user.company;
    let rows            = [];
    pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            //use the connection as normal
            var request = new Request(
                                  `SELECT  
                                            CONVERT(DECIMAL(10,2),SUM(A.Inventory-
                                            ISNULL([${db}].[dbo].UApps_Stock('5',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)-
                                            A.[Qty_ on SO]-
                                            ISNULL([${db}].[dbo].UApps_Stock('6',A.[Item No_],ISNULL(A.[Variant Code],'')  ,A.[Location Code],'',''),0)-
                                            A.[Qty_ on Transfer Out]-
                                            ISNULL([${db}].[dbo].UApps_Stock('7',A.[Item No_],ISNULL(A.[Variant Code],'')  ,A.[Location Code],'',''),0)-
                                           A. [Qty_ Sold not Posted]-
                                            ISNULL([${db}].[dbo].UApps_Stock('8',A.[Item No_],ISNULL(A.[Variant Code],'') ,A.[Location Code],'',''),0)))'actualStock'
                                    FROM [${db}].[dbo].[UAppsActualStock]A
                                    LEFT JOIN [${db}].[dbo].[${db}$Item] B
                                    ON A.[Item No_] = B.[No_]
    
                                    WHERE A.[Item No_]='${req.query.itemNo}' 
                                            AND A.[Department Code]='${dept_code}'
                                            AND A.[Location Code]='${req.query.locCode}'
                                     `



                                    , function(err, rowCount) {
                if (err) {
                    console.error(err);
                    return;
                }
                res.json({
                    success: true,
                    item: rows,
                    message: "Successful"
                });
                //release the connection back to the pool when finished
                connection.release();
            });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});


//Cek Data CustomerPrice
router.route('/cek-customer-price-sti/:_id/:_discGroup')
.get(passportLogin,(req, res) => {
    let dept_code       = req.user.dept_code;
    let db              = req.user.company;
    let itemNo          = req.params._id;
    let discGroup       = req.params._discGroup;
    let rows            = [];
    pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            //use the connection as normal
            var request = new Request(
                                    `SELECT CASE '${dept_code}'
                                      WHEN 'PDG'
                                        THEN  CASE '${discGroup}'
                                          WHEN 'CASH'
                                            THEN ISNULL(C.[Price 1],0)
                                          WHEN 'PDG_KRDT_M'
                                            THEN ISNULL(C.[Price 2],0)
                                          WHEN 'PDG_KRDT'
                                            THEN ISNULL(C.[Price 3],0)
                                          ELSE ISNULL(C.[Price 3],0)
                                          END                                      
                                      WHEN 'PKU'
                                      THEN CASE '${discGroup}'
                                        WHEN 'CASH'
                                          THEN ISNULL(C.[Price 2],0)
                                        WHEN 'PKU_M_CASH'
                                          THEN ISNULL(C.[Price 1],0) 
                                        WHEN 'PKU_M_KRDT'
                                          THEN ISNULL(C.[Price 3],0)
                                        ELSE ISNULL(C.[Price 3],0)
                                        END
                                      ELSE ISNULL(C.[Price 3],0)
                                      END AS 'SalesPrice'
                                 
                                    FROM [${db}].[dbo].[UAppsPrice]C
                                    WHERE
                                        C.[Item No_] ='${itemNo}' 
                                        AND C.[Dept Code]='${dept_code}'

                                 `
                            , function(err, rowCount) {
                if (err) {
                    console.error(err);
                    return;
                }
                res.json({
                    success: true,
                    item: rows,
                    message: "Successful"
                });
                //release the connection back to the pool when finished
                connection.release();
            });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

//Cek Data CustomerPrice
// router.route('/cek-customer-price/:_id/:_discGroup')
router.route('/cek-customer-price/:_id')
.get(passportLogin,(req, res) => {
    let dept_code       = req.user.dept_code;
    let db              = req.user.company;
    let itemNo          = req.params._id;
    // let discGroup       = req.params._discGroup;
    let rows            = [];
    pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            //use the connection as normal
            var request = new Request(
                                    `SELECT ISNULL(C.[Price 1],0)      AS 'SalesPrice'
                                 
                                    FROM [${db}].[dbo].[UAppsPrice]C
                                    WHERE
                                        C.[Item No_] ='${itemNo}' 
                                        AND C.[Dept Code]='${dept_code}'

                                 `
                            , function(err, rowCount) {
                if (err) {
                    console.error(err);
                    return;
                }
                res.json({
                    success: true,
                    item: rows,
                    message: "Successful"
                });
                //release the connection back to the pool when finished
                connection.release();
            });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

module.exports = router;