"use strict";

const router        = require('express').Router();
const config        = require('../middlewares/config-sql');
const passport      = require('passport');
const passportLogin = passport.authenticate('jwt', { session: false });
const getData       = require('../helpers/getData');

let Connection      = require('tedious').Connection;
let Request         = require('tedious').Request;
let ConnectionPool  = require('tedious-connection-pool');

let connection      = new Connection(config);
let poolConfig = {
    min: 2,
    max: 8,
    log: false
};
let pool = new ConnectionPool(poolConfig, config);

pool.on('error', function(err) {
    console.error(err);
});

//Get Customer Pagination
router.route('/price-discountOffer')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db              = req.user.company;
    let rows             = [];
    let FetchRows        = 20;
    let page             = parseInt(req.query[Object.keys(req.query)[0]]);
    let paramDesc        = req.query[Object.keys(req.query)[1]];
    let paramGetData     = '';
    let paramFix         = '';

    if (paramDesc ==null){
        paramFix   =`WHERE [Name] <>'' AND [Name] NOT LIKE '8840%' `
    }else{
        paramGetData = getData.getParam(paramDesc);
        paramFix=`WHERE [Name]+[Name 2]  LIKE '${paramGetData.toUpperCase()}' AND [Name] NOT LIKE '8840%' `
    }

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(`WITH Customer AS
                                    (
                                        SELECT  [RowNumber] = ROW_NUMBER() OVER (ORDER BY [Name]),
                                                [No_] as 'No',
                                                [Name] as 'Name',
                                                [Name 2] as 'Name2',
                                                [City] as'City',
                                                [Payment Terms Code] as 'Top',
                                                [Phone No_] as 'PhoneNo',
                                                [E-Mail] as 'Email',
                                                [Customer Disc_ Group] as 'DiscGroup'
                                                
                                        FROM [${db}].[dbo].[${db}$Customer]
                                        ${paramFix} 
                                    )
                                    SELECT *,
                                            (SELECT TOP 1 [RowNumber] FROM Customer ORDER BY [RowNumber] DESC) 'COUNT'
                                    FROM Customer
                                    WHERE
                                        [RowNumber] BETWEEN ((${page} - 1) * ${FetchRows} + 1)
                                        AND (((${page} - 1) * ${FetchRows}) + ${FetchRows})
                                     ORDER BY
                                        [RowNumber];`
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                customer: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

module.exports = router;