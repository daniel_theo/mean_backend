"use strict";

const router    = require('express').Router();
const User 		  = require('../models/user');
const config 	  = require('../middlewares/config');
const passport  = require('passport');
const async     = require("async");
const nodemailer = require("nodemailer");
const crypto    = require("crypto");

const passportLogin = passport.authenticate('jwt', { session: false });

const configSQL     = require('../middlewares/config-sql');
const getData       = require('../helpers/getData');

let Connection      = require('tedious').Connection;
let Request         = require('tedious').Request;
let ConnectionPool  = require('tedious-connection-pool');

let connection      = new Connection(configSQL);
let poolConfig = {
    min: 2,
    max: 8,
    log: false
};
let pool = new ConnectionPool(poolConfig, configSQL);

pool.on('error', function(err) {
    console.error(err);
});

router.route('/')
.get(passportLogin, (req, res) => {
	User.find({}, function(err, data){
		if (err) throw err;

		res.json({
        	success: true,
        	user: data,
        	message: "Successful"
      	});
	});
});

//Register
router.route('/signup')
.post(passportLogin, (req, res) => {
  req.checkBody('email','Email is Empty !!').notEmpty();
  req.checkBody('name','Name is Empty !!').notEmpty();
  req.checkBody('password','Password is Empty !!').notEmpty();
  req.checkBody('password1','Passwords do not match.').equals(req.body.password);

  let errors = req.validationErrors();
    if (errors) {
      return res.json({
            success: false,
            message: errors
      });
  }

  let user 			   = new User();
  user.name 			 = req.body.name;
  user.email 			 = req.body.email;
  user.password 	 = req.body.password;
  user.company	   = req.body.company;
  user.dept_code	 = req.body.dept_code;
  user.role        = req.body.role;
  user.salesPerson = req.body.salesPerson;
  user.retailUser  = req.body.retailUser;
  User.findOne({ email: req.body.email }, (err, existingUser) => {
    if (err) return  res.json({
            success: false,
            message: [{msg:'Failed '+err}]
          });
  		if (existingUser) {
    		return res.json({
      			success: false,
      			message: [{msg:'Account with that email is already exist'}]
    	});
  		} else {
    		user.save();
    		res.json({
      			success: true,
      			message: [{msg:'Success Register User'}]
    		});
  		}
 	});
});

router.route('/edit-user/:_id')
.get(passportLogin, (req, res) => {
  User.findOne({ _id: req.params._id }, (err, user) => {
      if (err)  return   res.json({
            success: false,
            message: [{msg:'Failed '+err}]
          });
      res.json({
          success: true,
          user: user,
          message: "Successful"
      });
  });
})
.post(passportLogin, (req, res) => {
  req.checkBody('email','Email is Empty !!').notEmpty();
  req.checkBody('name','Name is Empty !!').notEmpty();

  let errors = req.validationErrors();
    if (errors) {
      return res.json({
            success: false,
            message: errors
      });
  }
  User.findOne({ _id: req.params._id }, (err, user) => {
    if (err) return   res.json({
          success: false,
          message: [{msg:'Failed '+ err}]
    });
     
    user.name     = req.body.name;
    user.email    = req.body.email;
    user.company  = req.body.company;
    user.dept_code  = req.body.dept_code;
    user.role     = req.body.role;
    user.status   = req.body.status;
    user.salesPerson   = req.body.salesPerson;
    user.retailUser   = req.body.retailUser;

    user.save();
    res.json({
      success: true,
      message: 'Success Edit User'
    });
          
      
  });
})
.delete(passportLogin, (req, res) => {
  User.findByIdAndRemove({_id:req.params._id},(err, company) => {
    if (err) return res.json({  success: false,
                        message: 'Failed '+err});  
      res.json({
            success: true,
            message: 'Success Delete User '+ req.params._id
       });
  });
});

//Forgot Password
router.route('/forgot-password/:id')
.get(passportLogin, (req, res) => {
   async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      User.findOne({ _id: req.params.id }, function(err, user) {
        if (!user) {
          return res.json({
            success: false,
            message: [{msg:'User Id is not exist'}]
          });
        }
        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + (24*60*60*1000*30); // 30 hari
        user.save(function(err) {
          done(err, token, user);
        });
      });
    },
    function(token, user, done) {
      var smtpTransport = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
          user: 'notification.ur@gmail.com',
          pass: 'notification@ur90'
        }
      });
      var mailOptions = {
        to: user.email,
        from: 'notification.ur@gmail.com',
        subject: 'UApps Password Reset',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'https://172.18.29.110/forgot-password/' + token + '\n\n' +
          // 'https://172.18.29.121/forgot-password/'+ token + '\n\n' +
          'This link valid until: ' + user.resetPasswordExpires + '\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        res.json({
          success: true,
          message: 'Success Reset Password'
        });
        done(err, 'done');
      });
    }
  ], function(err) {
    if (err) return next(err);
    res.redirect('/accounts/forgot');
  });
});

router.route('/retail-user')
.get(passportLogin,(req, res) => {
    let db              = req.user.company;
    let rows            = [];
    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(`
                                  SELECT A.[ID]
                                  FROM [${db}].dbo.[${db}$Retail User] A
                                  WHERE A.[uApps Retail User] = 1
                                  `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                salesCode: rows,
                message: "Successful"
                
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

module.exports = router;