"use strict";
const router    = require('express').Router();
const User 		  = require('../models/user');
const config 	  = require('../middlewares/config');
const passport  = require('passport');

const passportLogin = passport.authenticate('jwt', { session: false });

//Reset Password
router.route('/:_id')
.get(passportLogin, (req, res) => {
	User.findOne({ _id: req.params._id }, (err, user) => {
    user.old_password = user.password; 
    user.save();

	    if (err)  return   res.json({
            success: false,
            message: [{msg:'Failed '+err}]
          });
	    res.json({
        	success: true,
        	user: user,
        	message: "Successful"
      	});
  	});
})
.post(passportLogin, (req, res) => {
  req.checkBody('password','Password is Empty !!').notEmpty();
  req.checkBody('confirm','Passwords do not match.').equals(req.body.password);

  let errors = req.validationErrors();
    if (errors) {
      return res.json({
            success: false,
            message: errors
      });
  }

	User.findOne({ _id: req.params._id }, (err, user) => {
	    if (err) return   res.json({
            success: false,
            message: [{msg:'Failed '+ err}]
          });
	    if (!user) {
	    	return res.json({
	        	success: false,
	        	message: [{msg:'Authenticated failed, User not found'}]
	      	});
	    } else if (user) {
        let validPassword = user.compareOldPassword(req.body.password);
         if (validPassword){
          return res.json({
              success: false,
              message: [{msg:'Password cannot same with last password'}]
          });
           }else{
          if(req.body.password !== req.body.confirm) {
            return res.json({
              success: false,
              message: [{msg:'Confirm password not Same'}]
            });
          }else{
            user.resetPasswordToken   = undefined;
            user.resetPasswordExpires = undefined;
            user.password       = req.body.password;
            user.save();
            res.json({
              success: true,
              message: 'Success Reset Password'
            });
          } 
        }  		
	    }
  	});
});

module.exports = router;