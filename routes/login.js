"use strict";

const router    = require('express').Router();
const jwt       = require('jsonwebtoken');
const User 		= require('../models/user');
const config 	= require('../middlewares/config');

router.post('/', (req, res, next) => {
	req.checkBody('email','Enter an email').notEmpty();
  	req.checkBody('password','Enter a password').notEmpty();

  	let errors = req.validationErrors();
  	if (errors) {
	  	if (errors.length > 1) {
		 	return res.json({
	      			success: false,
	      			message: [{msg:'Enter an email & Enter a password'}]
	    	});   
		} else {
			return res.json({
	      			success: false,
	      			message: errors
	    	});
		};
	}
	User.findOne({ email: req.body.email }, (err, user) => {
	    if (err)  return   res.json({
            success: false,
            message: 'Failed '+ err
          });

	    if (!user) {
	    	return res.json({
	        	success: false,
	        	message: [{msg:'Either your email/password is incorrect. Please try again'}]
	      	});
	      	
	    } else if (user) {
	    	var validPassword = user.comparePassword(req.body.password);
	      	if (!validPassword) {
	        	return res.json({
	          		success: false,
	          		message: [{msg:'Either your email/password is incorrect. Please try again'}]
	        	});

	      	} else {
	        	var token = jwt.sign({
	          		user: user
	        	}, config.jwtSecret, {
	          		expiresIn: '1d'
	        	});
	        	res.json({
	          		success: true,
	          		messageType :'success',
	          		message: 'Success',
	          		token: 'Bearer'+ ' '+token
	       	 	});
	      	}
	    }
  	});
});

module.exports = router;
