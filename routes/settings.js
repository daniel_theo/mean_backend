const router     	= require('express').Router();
const User 		   	= require('../models/user');
const Company 		= require('../models/company');
const DeptCode 		= require('../models/department');
const Role        = require("../models/role");
const validator 	= require('express-validator');
const passport   	= require('passport');
const fs          = require('fs');
const aclJson     = require("../middlewares/permission/acl.json");
const passportLogin = passport.authenticate('jwt', { session: false });

//Company
router.route('/company')
.get(passportLogin, (req, res) => {

  	Company.find({}, function(err, data){
   		if (err) return res.json({	success: false,
          							message: [{msg:'Failed '+err}]});    
        res.json({
        	success: true,
        	company:data,
          	message: "Successful"
        });
  });
})
.post(passportLogin, (req, res) => {

	req.checkBody('code','Code is Empty !!').notEmpty();
  req.checkBody('description','Description is Empty !!').notEmpty();

  let errors = req.validationErrors();
  	if (errors) {
	    return res.json({
      			success: false,
      			message: errors
    	});
	}
  	
  	let newCompany =({
    	code: req.body.code,
    	description: req.body.description,
    	address: req.body.address,
    	address1: req.body.address1,
    	phone: req.body.phone,
    	vatregistration: req.body.vatregistration,
    	vatname: req.body.vatname,
    	vataddress: req.body.vataddress,
    	vataddress1: req.body.vataddress1,
    	vatinvoiceauthorizedname: req.body.vatinvoiceauthorizedname,
    	vatinvoiceauthorizedposition: req.body.vatinvoiceauthorizedposition
  	});
  	Company.create(newCompany, function(err, newlyCreated){
  		if (err) return res.json({	success: false,
          							message: [{msg:'Failed '+err}]});   
     	res.json({
      			success: true,
      			message: 'Success Add Company '+ req.body.description
    		});
  	});
});

router.route('/company/:_id')
.get(passportLogin, (req, res) => {

  	Company.findOne({_id: req.params._id}, function(err, data){
   		if (err) return res.json({	success: false,
          							message: [{msg:'Failed '+err}]});  
        res.json({
        	success: true,
        	company:data,
          	message: "Successful"
        });
  });
})
.put(passportLogin, (req, res) => {
    req.checkBody('code','Code is Empty !!').notEmpty();
    req.checkBody('description','Description is Empty !!').notEmpty();

    let errors = req.validationErrors();
    if (errors) {
      return res.json({
            success: false,
            message: errors
      });
    }

	Company.findOne({_id:req.params._id},(err, company) => {
		if (err) return res.json({	success: false,
          							message: [{msg:'Failed '+err}]});  
	    if (!company) {
	    	res.json({
	        	success: false,
	        	message: [{msg:'Company Not found'}]
	      	});
	    } else if (company) {
	    	company.description= req.body.description;
		    company.address= req.body.address;
		    company.address1= req.body.address1;
		    company.phone= req.body.phone;
		    company.vatregistration= req.body.vatregistration;
		    company.vatname= req.body.vatname;
		    company.vataddress= req.body.vataddress;
		    company.vataddress1= req.body.vataddress1;
		    company.vatinvoiceauthorizedname= req.body.vatinvoiceauthorizedname;
		    company.vatinvoiceauthorizedposition= req.body.vatinvoiceauthorizedposition;
		    company.save();

		    res.json({
		        	success: true,
		        	message: 'Success Update Company '+ req.body.description
		     });
	    }
	});
})
.delete(passportLogin, (req, res) => {
	Company.findByIdAndRemove({_id:req.params._id},(err, company) => {
		if (err) return res.json({	success: false,
          							message: [{msg:'Failed '+err}]});  
	    res.json({
	        	success: true,
	        	message: 'Success Delete Company '+ req.params._id
	     });
	});
});


//Department
router.route('/department')
.get(passportLogin, (req, res) => {

  	DeptCode.find({}, function(err, data){
   		if (err) return res.json({	success: false,
          							message: [{msg:'Failed '+err}]});   
        res.json({
        	success: true,
        	dep:data,
          	message: "Successful"
        });
  });
})
.post(passportLogin, (req, res) => {

  	req.checkBody('code',' Code is Empty !!').notEmpty();
  	req.checkBody('description','Description is Empty !!').notEmpty();

  	let errors = req.validationErrors();
  	if (errors) {
	     return res.json({
      			success: false,
      			message: errors
    	});
	   }
    var newDept = ({
    	 code: req.body.code,
        description: req.body.description,
    });

    DeptCode.create(newDept, function(err, newlyCreated){
      	if (err) return res.json({	success: false,
          							message: [{msg:'Failed '+err}]});  
        res.json({
        	success: true,
          	message: 'Success Add Department Code '+ req.body.description
        });
    });
});


router.route('/department/:_id')
.get(passportLogin, (req, res) => {

  	DeptCode.findOne({_id: req.params._id}, function(err, data){
   		if (err) return res.json({	success: false,
          							message: [{msg:'Failed '+err}]});  
        res.json({
        	success: true,
        	department:data,
          	message: "Successful"
        });
  });
})
.put(passportLogin, (req, res) => {

  req.checkBody('code','Code is Empty !!').notEmpty();
  req.checkBody('description','Description is Empty !!').notEmpty();

  let errors = req.validationErrors();
  if (errors) {
     return res.json({
          success: false,
          message: errors
    });
   }

	DeptCode.findOne({_id:req.params._id},(err, department) => {
		if (err) return res.json({	success: false,
          							message: [{msg:'Failed '+err}]});  
	    if (!department) {
	    	res.json({
	        	success: false,
	        	message: 'Department Code Not found'
	      	});
	    } else if (department) {
	    	department.description= req.body.description;
	    	department.save();

		    res.json({
		        	success: true,
		        	message: 'Success Update Department Code '+ req.body.description
		     });
	    }
	});
})
.delete(passportLogin, (req, res) => {
	DeptCode.findByIdAndRemove({_id:req.params._id},(err, company) => {
		if (err) return res.json({	success: false,
          							message: [{msg:'Failed '+err}]});  
	    res.json({
	        	success: true,
	        	message: 'Success Delete Department Code '+ req.params._id
	     });
	});
});

//Role
router.route('/role')
.get(passportLogin, (req, res) => {
    Role.find({}, function(err, data){
      if (err) return res.json({ success: false,
                        message: [{msg:'Failed '+err}]});
        res.json({       
          success: true,
          roles:data,
          message: "Successful"
        });
  });
})

router.route('/add-role')
.get(passportLogin, (req, res) => {
  let access    = aclJson;
  let codeRole  = 'administrator'

  let roleGroup =access.filter((data) => data.group === codeRole);
  res.json({
            success: true,
            roleGroup: roleGroup
  });
})
.post(passportLogin, (req, res) => {
 
  req.checkBody('code','Code is Empty !!').notEmpty();
  req.checkBody('description','Description is Empty !!').notEmpty();

  let errors = req.validationErrors();
  if (errors) {
    return res.json({
            success: false,
            message: errors
      });
  }

  let newRole = ({
    code: req.body.code,
    description: req.body.description,
  });

  Role.create(newRole, function(err, newlyCreated){
      if (err) throw res.json({ success: false,
                          message: err});  
      var dataTable = fs.readFileSync("./middlewares/permission/acl.json");
      var arrResource = Object.keys(req.body.resource);
      var arrMethod = Object.keys(req.body.methods);
      var code = [];
      code.push(req.body.code);
      var data = {};
      var arr=[];
      
      data.permissions=[];
      code.forEach(function(k){
        data.group = k;
        arrResource.forEach(function(i){
          var httpMethod=[];
          httpMethod.push(req.body.methods[i]);

            if (i !== "") {
              var objhttps=[];
              req.body.methods[i].split(";").forEach(function(l){
                if (l !== "") {
                  var objTemp={
                    http:l.toUpperCase()
                  }
                }else{
                  return;
                }
                objhttps.push(objTemp);
              })
              var obj={
                resource:i
                ,methods :objhttps
              }
            }else{
              return;
            }
            data.permissions.push(obj);
        })
        try {
          var dataTable = fs.readFileSync("./middlewares/permission/acl.json");
          arr = JSON.parse(dataTable);
        } catch (e) {
          return;
        }
        var duplicateGroup = arr.filter((data) => data.group === k);
        if (duplicateGroup == 0) {
           arr.push(data);
           fs.writeFileSync("./middlewares/permission/acl.json", JSON.stringify(arr));
        }
      })
      res.json({
        success: true,
        message: 'Success Update Role '+ req.body.code
      });
  });
});


router.route('/show-role/:_id')
.get(passportLogin, (req, res) => {
  Role.findOne({_id:req.params._id},function (err,data) {
    if (err) return res.json({ success: false,
                        message: [{msg:'Failed '+err}]});
    var access = aclJson;
    var access2 = aclJson;
    var codeRole = data.code;

    var roleGroup =access2.filter((data) => data.group === codeRole);
    res.json({
              success: true,
              roleGroup: roleGroup,
              code : data.code,
              description : data.description
    });
  });
});
 
router.route('/edit-role/:_id')
.get(passportLogin, (req, res) => {
  Role.findOne({_id:req.params._id},function (err,data) {
    if (err) return res.json({ success: false,
                        message: [{msg:'Failed '+err}]});
    var access = aclJson;
    var codeRole = data.code;

    var code = data.code;
    var dataTable = fs.readFileSync("./middlewares/permission/acl.json");
    arr = JSON.parse(dataTable);
    var getData = arr.filter((data) => data.group !== code);
    delete getData;
    fs.writeFileSync("./middlewares/permission/acl.json", JSON.stringify(getData));

    var adminGroup =access.filter((data) => data.group === "administrator");
    res.json({
              success: true,
              roleGroup: adminGroup,
              code : data.code,
              description : data.description
    });
  });
})
.post(passportLogin, (req, res) => {
// Edit roleuser Process
  Role.findOne({
    _id:req.params._id
  })
  .then (role => {
    role.description= req.body.description;
    role.save()
    .then(role => {
      var data = {}
      var arr=[]
      data.permissions=[]
      var arrResource = Object.keys(req.body.resource);
      var arrMethod = Object.keys(req.body.methods);
      var code = [];
      code.push(req.body.code);

      code.forEach(function(k){
          data.group = k;
          arrResource.forEach(function(i){
            var httpMethod=[];
            httpMethod.push(req.body.methods[i]);

              if (i !== "") {
              var objhttps=[];
              req.body.methods[i].split(";").forEach(function(l){
                if (l !== "") {
                  var objTemp={
                    http:l.toUpperCase()
                  }
                }else{
                  return;
                }
                objhttps.push(objTemp);
              })
              var obj={
                resource:i
                ,methods :objhttps
              }
            }else{
              return;
            }
            data.permissions.push(obj);
          })

           try {
              var dataTable = fs.readFileSync("./middlewares/permission/acl.json");
              arr = JSON.parse(dataTable);
            } catch (e) {
              return;
            }
            var duplicateGroup = arr.filter((data) => data.group === k);
            if (duplicateGroup == 0) {
               arr.push(data);
               fs.writeFileSync("./middlewares/permission/acl.json", JSON.stringify(arr));
            }
          })   
     res.json({
        success: true,
        message: 'Success Update Role '+ req.body.code
      });
    });
  });

});

router.route('/role/:_id')
.delete(passportLogin, (req, res) => {
  Role.findByIdAndRemove({_id:req.params._id}, function(err, deleted){
    if (err) return res.json({ success: false,
                              message: [{msg:'Failed '+err}]}); 
    var code = deleted.code;
    var dataTable = fs.readFileSync("./middlewares/permission/acl.json");
    arr = JSON.parse(dataTable);
    var getData = arr.filter((data) => data.group !== code);
    delete getData;
    fs.writeFileSync("./middlewares/permission/acl.json", JSON.stringify(getData));
    res.json({
      success: true,
      message: 'Success Delete Role '+ code
    });
  });
});
module.exports = router;
