"use strict";

const router    = require('express').Router();
const User 		= require('../models/user');
const passport 	= require('passport');

const passportLogin = passport.authenticate('jwt', { session: false });

router.route('/')
.get(passportLogin, (req, res) => {
	// User.findOne({ _id: req.decoded._id }, function(err, data){
	// 	if (err)  return  res.json({
 //            success: false,
 //            message: 'Failed '+ err
 //          });
		
	// 	res.json({
 //        	success: true,
 //        	user: data,
 //        	message: "Successful"
 //      	});
	// });
  try{
      User.findOne({ _id: req.decoded['_id'] },['name', 'email', 'role', 'dept_code', 'salesPerson','company'], (err, data)=>{
        if (err)  return  res.status(404).send({
          success: false,
          message: 'Failed '+ err
          });
        res.status(200).send({
          success: true,
          user: data
        });
      });
    }catch(error){
      return res.status(400).send({
        success: false,
        message: error
      });
    }
});


module.exports = router;
