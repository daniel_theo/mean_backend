"use strict";

const router        = require('express').Router();
const config        = require('../middlewares/config-sql');
const passport      = require('passport');
const passportLogin = passport.authenticate('jwt', { session: false });
const getData       = require('../helpers/getData');

let Connection      = require('tedious').Connection;
let Request         = require('tedious').Request;
let ConnectionPool  = require('tedious-connection-pool');

let connection      = new Connection(config);
let poolConfig = {
    min: 2,
    max: 8,
    log: false
};
let pool = new ConnectionPool(poolConfig, config);

pool.on('error', function(err) {
    console.error(err);
});

//cek Customer DiscGroup
router.route('/check-disc-group/:_id')
.get(passportLogin,(req, res) => {
    let dept_code       = req.user.dept_code;
    let db              = req.user.company;
    let rows            = [];

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(`
                                        SELECT  
                                                [No_] as 'No',
                                                [Customer Disc_ Group] as 'DiscGroup'
                                        FROM [${db}].[dbo].[${db}$Customer]
                                        where [No_] = ${req.params._id}
                                   `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({             
                success: true,
                customer: rows,
                message: "Successful"      
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});


//Get Item Pagination
router.route('/list-customer-scrolling-no')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db              = req.user.company;
    let rows             = [];
    let FetchRows        = 20;
    let paramDesc        = req.query[Object.keys(req.query)[0]];
    let paramGetData     = '';
    let paramFix         = '';

    if (paramDesc ==null){
        paramFix   =`WHERE [Name] <>'' AND [Name] NOT LIKE '8840%' `
    }else{
       	paramGetData = getData.getParam(paramDesc);
        paramFix=`WHERE [Name]+[Name 2]  LIKE '${paramGetData.toUpperCase()}' AND [Name] NOT LIKE '8840%' `
     }

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(
        							`WITH Customer AS
                                    (
                                        SELECT  [RowNumber] = ROW_NUMBER() OVER (ORDER BY [Name] ASC)
                                        FROM [${db}].[dbo].[${db}$Customer]
                                        ${paramFix} 
                                    )
                                    SELECT TOP 1 (SELECT TOP 1 [RowNumber] FROM Customer ORDER BY [RowNumber] DESC) 'RowNumber'
                                    FROM Customer B
                      				GROUP BY
                                        B.[RowNumber]
                                    ORDER BY
                                        B.[RowNumber] DESC
                                      `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
             res.json({
                success: true,
                customer: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

//Get Item Pagination
router.route('/list-customer-scrolling')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db              = req.user.company;
    let rows             = [];
    let FetchRows        = 20;
    let page             = parseInt(req.query[Object.keys(req.query)[0]]);

    let paramDesc        = req.query[Object.keys(req.query)[1]];
    let paramGetData     = '';
    let paramFix         = '';
    if (paramDesc == ""){
        paramFix   =`WHERE [Name] <>'' AND [Name] NOT LIKE '8840%' `
    }else{
       	paramGetData = getData.getParam(paramDesc);
        paramFix=`WHERE [Name]+[Name 2]  LIKE '${paramGetData.toUpperCase()}' AND [Name] NOT LIKE '8840%' `
     }

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(`WITH Customer AS
                                    (
                                        SELECT  [RowNumber] = ROW_NUMBER() OVER (ORDER BY [Name]),
                                                [No_] as 'No',
                                                [Name] as 'Name',
                                                [Name 2] as 'Name2',
                                                [City] as'City',
                                                [Payment Terms Code] as 'Top',
                                                [Phone No_] as 'PhoneNo',
                                                [E-Mail] as 'Email',
                                                [Customer Disc_ Group] as 'DiscGroup'
                                                
                                        FROM [${db}].[dbo].[${db}$Customer]
                                        ${paramFix} 
                                    )
                                    SELECT *,
                                            (SELECT TOP 1 [RowNumber] FROM Customer ORDER BY [RowNumber] DESC) 'COUNT'
                                    FROM Customer
                                    WHERE
                                        [RowNumber] BETWEEN (${page}) AND(${page}+19)
                                     ORDER BY
                                        [RowNumber];`
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
             res.json({
                success: true,
                customer: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});



//Get Customer Pagination
router.route('/list-customer-pagination')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db              = req.user.company;
    let rows             = [];
    let FetchRows        = 20;
    let page             = parseInt(req.query[Object.keys(req.query)[0]]);
    let paramDesc        = req.query[Object.keys(req.query)[1]];
    let paramGetData     = '';
    let paramFix         = '';
    if (paramDesc ==null){
        // paramFix   =''
        paramFix   =`WHERE [Name] <>'' AND [Name] NOT LIKE '8840%' `
    }else{
        paramGetData = getData.getParam(paramDesc);
        paramFix=`WHERE [Name]+[Name 2]  LIKE '${paramGetData.toUpperCase()}' AND [Name] NOT LIKE '8840%' `
     }

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(`WITH Customer AS
                                    (
                                        SELECT  [RowNumber] = ROW_NUMBER() OVER (ORDER BY [Name]),
                                                [No_] as 'No',
                                                [Name] as 'Name',
                                                [Name 2] as 'Name2',
                                                [City] as'City',
                                                [Payment Terms Code] as 'Top',
                                                [Phone No_] as 'PhoneNo',
                                                [E-Mail] as 'Email',
                                                [Customer Disc_ Group] as 'DiscGroup'
                                                
                                        FROM [${db}].[dbo].[${db}$Customer]
                                        ${paramFix} 
                                    )
                                    SELECT *,
                                            (SELECT TOP 1 [RowNumber] FROM Customer ORDER BY [RowNumber] DESC) 'COUNT'
                                    FROM Customer
                                    WHERE
                                        [RowNumber] BETWEEN ((${page} - 1) * ${FetchRows} + 1)
                                        AND (((${page} - 1) * ${FetchRows}) + ${FetchRows})
                                     ORDER BY
                                        [RowNumber];`
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                customer: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

//Get Master Top
router.route('/top')
.get(passportLogin,(req, res) => {
    let dept_code       = req.user.dept_code;
    let db              = req.user.company;
    let rows            = [];

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(`
                                        SELECT Code as 'Code',
                                                [Due Date Calculation] as'Calculation',
                                                Description as 'Description'
                                        FROM [${db}].[dbo].[${db}$Payment Terms]
                                   `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            // res.send(rows);
            res.json({
                success: true,
                item: rows,
                message: "Successful"
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});


//Get Master salesPerson
router.route('/salesPerson')
.get(passportLogin,(req, res) => {
    let dept_code       = req.user.dept_code;
    let db              = req.user.company;
    let rows            = [];

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(`                          
                                    SELECT '' as 'code','' as 'name'
                                    UNION ALL
                                    SELECT  Code,
                                            Name
                                    FROM  [${db}].[dbo].[${db}$Salesperson_Purchaser]
                                    WHERE [Global Dimension 1 Code] = '${dept_code}'
                                   `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                salesCode: rows,
                message: "Successful"
                
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

router.route('/salesPerson-all')
.get(passportLogin,(req, res) => {
    // let dept_code       = req.user.dept_code;
    let db              = req.user.company;
    let rows            = [];

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(`                          
                                     SELECT '' as 'code','' as 'name'
                                    UNION ALL
                                    SELECT  Code,
                                            Name
                                    FROM  [${db}].[dbo].[${db}$Salesperson_Purchaser]

                                    
                                   `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                salesCode: rows,
                message: "Successful"
                
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

//Get PostCodes
router.route('/post-codes')
.get(passportLogin,(req, res) => {
    let dept_code        = req.user.dept_code;
    let db              = req.user.company;
    let rows            = [];

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(`                          
                                   SELECT A.Code+'='+A.City as 'Code',A.City 
                                    FROM [${db}].[dbo].[${db}$Post Code]A
                                    LEFT JOIN [${db}].[dbo].[${db}$County] B
                                    ON A.County = B.Name
                                    where A.Code like 'KC%' 
                                        AND B.Description = '${dept_code}'
                                    ORDER BY A.City
                                   `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                postCodes: rows,
                message: "Successful"
                
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});


//Get county
router.route('/county')
.get(passportLogin,(req, res) => {
    let db              = req.user.company;
    let rows            = [];

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(`  
                                    SELECT A.Name, A.Description
                                    FROM [${db}].[dbo].[${db}$County] A
                                    where A.Name like 'BC%'                        
                                   `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                county: rows,
                message: "Successful"
                
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

//Get ship address
router.route('/delivery-address/:_id')
.get(passportLogin,(req, res) => {
    let db              = req.user.company;
    let rows            = [];

    pool.acquire(function (err, connection) {
        if (err) {
            console.error(err);
            return;
        }
        var request = new Request(`                          
                                    SELECT 	A.[Code] as 'Code',
                                    		A.[Name] as 'Name',
                                    		A.[Name 2] as 'Name2',
                                    		A.[Address] as 'Address',
                                    		A.[Address 2] as 'Address 2',
                                    		A.[City] as 'City',
                                    		A.[Phone No_] as 'PhoneNo',
                                            A.[Contact] as 'Contact',
                                            B.Description 'county'
                                    FROM  [${db}].[dbo].[${db}$Ship-to Address]A
                                    LEFT JOIN [${db}].[dbo].[${db}$County] B
                                            ON A.[County] = B.Name
                                    where [Customer No_] = ${req.params._id}
                                   `
            , function(err, rowCount) {
            if (err) {
                console.error(err);
                return;
            }
            res.json({
                success: true,
                address: rows,
                message: "Successful"
                
            });
            //release the connection back to the pool when finished
            connection.release();
        });

        request.on('row', function(columns) {
            var row = {};
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value;

            });
            rows.push(row);
        });
        connection.execSql(request);
    });
});

//Get ship address
router.route('/add-delivery-address')
.post(passportLogin,(req, res) => {
    let db              = req.user.company;
    let rows            = [];
    try{
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            var request = new Request(`                          
                                         INSERT INTO [${db}].[dbo].[${db}$Ship-to Address]
                                    (
                                        [Customer No_],
                                        [Code],
                                        [Name],
                                        [Name 2],
                                        [Address],
                                        [Address 2],
                                        [City],
                                        [Contact],
                                        [Phone No_],
                                        [Telex No_],
                                        [Shipment Method Code],
                                        [Shipping Agent Code],
                                        [Place of Export],
                                        [Country_Region Code],
                                        [Last Date Modified],
                                        [Location Code],
                                        [Fax No_],
                                        [Telex Answer Back],
                                        [Post Code],
                                        [County],
                                        [E-Mail],
                                        [Home Page],
                                        [Tax Area Code],
                                        [Tax Liable],
                                        [Shipping Agent Service Code],
                                        [Service Zone Code]
                                    )
                                    VALUES(
                                        '${req.body.custNo}', --[Customer No_],
                                        '${req.body.code}',--[Code],
                                        '${req.body.name}',--[Name],
                                        '',--[Name 2],
                                        '${req.body.address}',--[Address],
                                        '${req.body.address2}',--[Address 2],
                                        '${req.body.city}',--[City],
                                        '',--[Contact],
                                        '${req.body.phone}',--[Phone No_],
                                        '',--[Telex No_],
                                        '',--[Shipment Method Code],
                                        '',--[Shipping Agent Code],
                                        '',--[Place of Export],
                                        '',--[Country_Region Code],
                                        CONVERT(date, CAST(CONVERT(date, GETDATE()) AS datetime2)),--[Last Date Modified],
                                        '',--[Location Code],
                                        '',--[Fax No_],
                                        '',--[Telex Answer Back],
                                        '${req.body.postCode}',--[Post Code],
                                        '${req.body.county}',--[County],
                                        '',--[E-Mail],
                                        '',--[Home Page],
                                        '',--[Tax Area Code],
                                        0,--[Tax Liable],
                                        '',--[Shipping Agent Service Code],
                                        ''--[Service Zone Code]

                                    );
                                       `
                , function(err, rowCount) {
                if (err) {
                    console.error(err);
                    return;
                }
                res.json({
                    success: true,               
                    message: "Successful"
                    
                });
                //release the connection back to the pool when finished
                connection.release();
            });
            connection.execSql(request);
        });
    }catch(err){
        console.log(err);
        let e = JSON.stringify(err, ["message", "arguments", "type", "name"]); 

        return {error: JSON.parse(e).message};
    } 
});


module.exports = router;